import Chart, { ChartConfiguration, ChartDataSets } from 'chart.js';
import ChartDataLabels from 'chartjs-plugin-datalabels';

import fs from 'fs';
import { ChartJSNodeCanvas } from 'chartjs-node-canvas';

// Register the plugin to all charts:
Chart.plugins.register(ChartDataLabels as any);

const labelsOut = ['Alain PP','Veronique PP',
    'Alain PP', 'Veronique NP', 'Alain US',
    'Alain PP', 'Veronique NP', 'Alain US',
    'Alain PP', 'Veronique PP'];
const labelsIn = ['Entreprise','Immobilier','Financier','Autres'];

const width = 400; //px
const height = 400; //px
const canvasRenderService = new ChartJSNodeCanvas({width, height});

(async () => {
    const colors1 = ['#2f7ed8', '#0d233a', '#8bbc21', '#910000']
    const colors2 = [
        '#FF3784',
        '#36A2EB',
        '#4BC0C0',
        '#F77825',
        '#9966FF',
        '#00A8C6',
        '#379F7A',
        '#CC2738',
        '#8B628A',
        '#8FBE00',
    ];
    const conf: any = {
        type: 'doughnut',
        data: {
            datasets: [{
                backgroundColor: colors2,
                data: [40, 10, 10, 20, 20, 10, 20, 20, 25, 25],
                labels: labelsOut,
                datalabels: {
                    anchor: 'center',
                    backgroundColor: null,
                    borderWidth: 0,
                    labels: {
                        title: {
                            color: 'yellow'
                        }
                    }
                }
            }, {
                backgroundColor: colors1,
                data: [50, 50, 50, 50],
                labels: labelsIn,
                datalabels: {
                    anchor: 'center',
                    backgroundColor: null,
                    borderWidth: 0,
                    labels: {
                        value: {
                            color: 'green'
                        }
                    }
                }
            }],
            labels: labelsOut.concat(labelsIn)
        },
        options: {
            legend: {
                position: 'left',
                labels: {
                    generateLabels: () => {
                        let labels = [];
                        conf.data.datasets.forEach((ds, iDs) => labels = labels.concat(ds.labels.map((l, iLabel) => ({
                            datasetIndex: iDs,
                            labelIndex: iLabel,
                            text: l,
                            fillStyle: ds.backgroundColor[iLabel],
                            hidden: false,
                            strokeStyle: '#fff'
                        }))));
                        return labels;
                    }
                }
            },
            plugins: {
                /* legend: false,
                outlabels: {
                    text: '%l',
                    color: 'black',
                    stretch: 45,
                    font: {
                        resizable: true,
                        minSize: 12,
                        maxSize: 18
                    }
                },*/
                datalabels: {
                    backgroundColor: function(context) {
                        return context.dataset.backgroundColor;
                    },
                    borderColor: 'white',
                    borderRadius: 25,
                    borderWidth: 2,
                    color: 'white',
                    display: function(context) {
                        var dataset = context.dataset;
                        var count = dataset.data.length;
                        var value = dataset.data[context.dataIndex];
                        return false;
                    },
                    font: {
                        weight: 'bold'
                    },
                    padding: 6,
                    formatter: Math.round
                }
            },

            // Core options
            aspectRatio: 4 / 3,
            cutoutPercentage: 32,
            layout: {
                padding: 32
            },
            elements: {
                line: {
                    fill: false
                },
                point: {
                    hoverRadius: 7,
                    radius: 5
                }
            },
        }
    };

    const imageBuffer = await canvasRenderService.renderToBuffer(conf, "image/png");

    // Write image to file
    fs.writeFileSync('mychart.png', imageBuffer);
})();
